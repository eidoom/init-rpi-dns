# [init-rpi-dns](https://gitlab.com/eidoom/init-rpi-dns)

[Companion post](https://computing-blog.netlify.app/post/new-dns/#system)

## Usage

Local
```shell
passwd
useradd -m ryan
passwd ryan
ip a
```

Remote
```shell
ssh ryan@192.168.1.*
```
```shell
chsh -s /bin/bash
exit
```
(Log out and in to apply chsh.)
```shell
ssh ryan@192.168.1.*
su
```
(Maybe the following would be better done in `/root`.)
```shell
apt update
apt install git
git clone https://gitlab.com/eidoom/init-rpi-dns.git
cd init-rpi-dns
./init.sh
EDITOR=vi visudo  # set `%sudo     ALL=(ALL:ALL) NOPASSWD: ALL`
```
Fill `~/.ssh/authorized_keys`.
Poweroff Pi and move to new home.
