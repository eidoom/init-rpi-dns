#!/usr/bin/env bash

echo 'PATH="/usr/sbin:$PATH"' >> ~/.profile
source ~/.profile
echo "deb http://deb.debian.org/debian bullseye-updates main contrib non-free" >> /etc/apt/sources.list
echo "deb http://deb.debian.org/debian bullseye-backports main contrib non-free" >> /etc/apt/sources.list
apt install sudo
usermod -aG sudo ryan
sed -i "s/^#PasswordAuthentication yes$/PasswordAuthentication no/g" /etc/ssh/sshd_config
mkdir /home/ryan/.ssh
touch /home/ryan/.ssh/authorized_keys
systemctl disable wpa_supplicant
echo "auto eth0
iface eth0 inet static
  address 192.168.1.3/24
  gateway 192.168.1.1" > /etc/network/interfaces.d/eth0
cd /home/ryan
git clone https://gitlab.com/eidoom/conf-unbound.git
cd conf-unbound
./init.sh
cd /home/ryan/init-rpi-dns
chown -R ryan:ryan /home/ryan/
